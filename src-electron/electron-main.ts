import { app, BrowserWindow, ipcMain } from 'electron';
import path from 'path';
import * as remoteMain from '@electron/remote/main';
remoteMain.initialize();


/*
try {
  if (process.platform === 'win32' && nativeTheme.shouldUseDarkColors === true) {
    require('fs').unlinkSync(require('path').join(app.getPath('userData'), 'DevTools Extensions'))
  }
} catch (_) { }
*/

let mainWindow: BrowserWindow | null;
let plotWindow: BrowserWindow | null;

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    useContentSize: true,
    frame: false,
    icon: path.join(__dirname, 'icons/linux-512x512.png'),
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
      contextIsolation: true,
      // More info: /quasar-cli/developing-electron-apps/electron-preload-script
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD ?? ''),
    },
  });

  mainWindow.loadURL(process.env.APP_URL ?? '').catch((err) => console.log(err));

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    //mainWindow.webContents.openDevTools();
  } else {
    // we're on production; no access to devtools pls
/*     mainWindow.webContents.on('devtools-opened', () => {
      mainWindow?.webContents.closeDevTools();
    }); */
  }

  mainWindow.maximize();

  mainWindow.on('closed', () => {
    mainWindow = null;
    app.quit();
  });
  remoteMain.enable(mainWindow.webContents);
}

const createPlotWindow = () => {
  plotWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    useContentSize: true,
    frame: false,
    icon: path.join(__dirname, 'icons/linux-512x512.png'),
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
      contextIsolation: true,
      // More info: /quasar-cli/developing-electron-apps/electron-preload-script
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD ?? ''),
    },
  })
  if (process.env.APP_URL) {
    const modalPath = process.env.NODE_ENV === 'development'
    ? `${process.env.APP_URL}/#/plot`
    : `file://${__dirname}/index.html#plot`
    plotWindow.loadURL(modalPath ?? '').catch((err) => console.log(err));
  }
  plotWindow.setAlwaysOnTop(true);

  plotWindow.on('closed', function () {
    plotWindow = null
  })
  remoteMain.enable(plotWindow.webContents);
  return plotWindow;
}

app.on('ready', () => {
  createWindow();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

// ------

import { listFiles, readFile, writeFile, copyFile, getPathFromString } from './io/fsutils';

interface ListDirOptions {
  path: string;
  extensions: Array<string>;
}

ipcMain.handle('listDir', async (event, arg: ListDirOptions) => {
  return await listFiles(arg.path, arg.extensions)
    .then((files) => files)
    .catch((err) => {
      console.log(err);
    });
});

ipcMain.handle('readFile', (event, arg: string) => {
  return readFile(arg)
    .then((content) => content)
    .catch((err) => {
      console.log(err);
    });
});

interface WriteFileOptions {
  path: string;
  content: string;
}

ipcMain.handle('writeFile', async (event, arg: WriteFileOptions) => {
  return await writeFile(arg.path, arg.content)
    .then((aa) => aa)
    .catch((err) => console.log(err));
});

interface CopyFileOptions {
  source: string;
  destination: string;
}

ipcMain.handle('copyFile', async (event, arg: CopyFileOptions) => {
  return await copyFile(arg.source, arg.destination)
    .then((aa) => aa)
    .catch((err) => console.log(err));
});

import { deleteTask, listTasks, readTask, renameTask, saveTask } from './io/task';
import { TaskFile } from 'src/models/files';

ipcMain.handle('task:write', (event, arg: TaskFile) => {
  return saveTask(arg);
});
ipcMain.handle('task:list', (event) => {
  return listTasks();
});
ipcMain.handle('task:read', (event, arg: string) => {
  return readTask(arg);
});
ipcMain.handle('task:rename', (event, arg1, arg2) => {
  return renameTask(arg1, arg2);
});
ipcMain.handle('task:delete', (event, arg) => {
  return deleteTask(arg);
});

import * as chokidar from 'chokidar';

chokidar
  .watch(getPathFromString('task'))
  .on('unlink', (filePath) => {
    mainWindow?.webContents.send('task:removed', path.parse(filePath).name);
  })
  .on('add', (filePath) => {
    mainWindow?.webContents.send('task:added', path.parse(filePath).name);
  });

// WEBSOCKET ------------------------------------------------------------------

import {
  getHWInfo,
  getFWVersion,
  getControllersList,
  getReferencesList,
  getControllerParams,
  getReferenceParams,
  getControllerLogs,
  getReady,
  setController,
  setHWLoggables,
  setControllerLoggables,
  setReference,
  startLoop,
  standby,
} from './serial/coms';

ipcMain.handle('ws:get_hw_info', (event) => {
  return getHWInfo();
});

ipcMain.handle('ws:get_fw_version', (event) => {
  return getFWVersion();
});

ipcMain.handle('ws:get_ctrls_list', (event) => {
  return getControllersList();
});

ipcMain.handle('ws:get_refs_list', (event) => {
  return getReferencesList();
});

ipcMain.handle('ws:get_ctrl_params', (event, ctrl_id: string) => {
  return getControllerParams(ctrl_id);
});

ipcMain.handle('ws:get_ref_params', (event, ref_id: string) => {
  return getReferenceParams(ref_id);
});

ipcMain.handle('ws:get_ctrl_logs', (event, ctrl_id: string) => {
  return getControllerLogs(ctrl_id);
});

ipcMain.handle('ws:ready', (event) => {
  return getReady();
})

ipcMain.handle('ws:set_controller', (event, id_ctrl: string, motor: number, parameters: number[]) => {
  return setController(id_ctrl, motor, parameters);
});

ipcMain.handle('ws:set_hw_loggables', (event, loggablesHW: string[]) => {
  return setHWLoggables(loggablesHW);
});

ipcMain.handle('ws:set_ctrl_loggables', (event, motorNumber: number, loggablesControllers: string[]) => {
  return setControllerLoggables(motorNumber, loggablesControllers);
});

ipcMain.handle('ws:set_reference', (event, id_ref: string, motor: number, parameters: number[]) => {
  return setReference(id_ref, motor, parameters);
});

ipcMain.handle('ws:start_loop', (event, frequency: number, duration: number) => {
  return startLoop(frequency, duration);
});

ipcMain.handle('ws:standby', (event) => {
  return standby();
});

ipcMain.handle('open:plot', (event) => {
  if (plotWindow == null)
    createPlotWindow();
})

import { serial } from './serial/coms'

import { DirectoryManager } from './io/fsutils'

const directoryManager = new DirectoryManager();
directoryManager.init();


import { log_manager } from './logger/manager'

ipcMain.handle('plot:req_headers', (event) => {
  return new Promise((resolve, reject) => {
    resolve(log_manager?.currentExperiment.currentTask.headers ?? []);
  });
})

/* import { LogManager } from './logger/manager'

let logManager: LogManager;

ipcMain.handle('log:init', (event, experimentName: string) => {
  logManager = new LogManager(experimentName);
});

ipcMain.handle('log:save', (event, fileName: string, headers: Array<string>) => {
  logManager.save(fileName, headers);
});

ipcMain.handle('log:init_plot', (event) => {
  return new Promise((resolve, reject) => {
    resolve(['Time', 'Hello', 'Hello2', 'Hello3', 'Hello4', 'Hello5']);
  });
})
 */

/* let counter = 0;
setInterval(() => {
  plotWindow?.webContents.send('log:data', [counter, Math.random(), Math.random(), Math.random(), Math.random(), Math.random()]);
  counter += 1;
}, 30); */

export { mainWindow, plotWindow };

import { glob } from 'glob';

import { parseFile } from '@fast-csv/parse';
import { CSVOptions } from 'src/models/logs'
const readCsv = (path, options) => {
  return new Promise((resolve, reject) => {
    const result: CSVOptions = {
      'headers': [],
      'data': []
    };
    let headers = false;
    parseFile(path, options)
      .on("error", reject)
      .on("data", (row) => {
        if (headers)
          result.data.push(row.map(Number))
        else {
          result.headers = row;
          headers = true;
        }
      })
      .on("end", () => {
        resolve(result);
      });
  });
}

ipcMain.handle('library:list_experiments', (event, someArgument) => {
  const experiments = glob.sync(getPathFromString('log') +"/*/", {nosort: true});
  return experiments.map(dir_path => {
    return {
      name: path.basename(dir_path),
      path: dir_path,
      logs: glob.sync("*.csv", {cwd: dir_path})
    }
  })
})

ipcMain.handle('library:list_tasks', (event, experimentName) => {
  return glob.sync("*.csv", {cwd: '/' + experimentName}).sort((a,b) => a.localeCompare(b, undefined, { numeric: true, sensitivity: 'base' }));
})

ipcMain.handle('library:read_csv', async (event, csv_path) => {
  return await readCsv(csv_path, {});
});
