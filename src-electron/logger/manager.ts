import { ipcMain } from 'electron';
import { plotWindow } from '../electron-main';
import { getPathFromString } from '../io/fsutils';
import { LogFile } from './logfiles';
import * as fs from 'fs';
import * as path from 'path';

import { ExperimentOptions, TaskOptions } from '../../src/models/logs';
class LogManager {
  private _experiment: Experiment;
  private _directory: string;

  public get currentExperiment(): Experiment {
    return this._experiment;
  }

  constructor(options: ExperimentOptions) {
    this._experiment = new Experiment(options);
    this._directory = path.join(getPathFromString('log'), this._experiment.name);
  }

  private mkdir() {
    if (!fs.existsSync(this._directory)) {
      fs.mkdirSync(this._directory, { recursive: true });
    }
  }

  public push(data: Array<number>) {
    this._experiment.currentTask.pushData(data);
  }

  public next() {
    return this._experiment.next();
  }

  public export() {
    this.mkdir();
    this._experiment.export().forEach((task, taskIndex) => {
      const logFile = new LogFile({
        headers: task.headers,
        path: path.join(this._directory, `log${taskIndex}.csv`),
      });
      void logFile.create(task.export());
    });
  }
}

class Experiment {
  private _tasks: Array<Task>;
  private _index: number;
  private _options: ExperimentOptions;

  public get currentTask(): Task {
    return this._tasks[this._index];
  }

  public get name(): string {
    return this._options.experimentName;
  }

  constructor(options: ExperimentOptions) {
    this._tasks = Array<Task>();
    this._index = -1;
    this._options = options;
  }

  public next() {
    if (this._index + 1 >= this._options.length) return -1;
    this._index += 1;
    this._tasks.push(new Task(this._options.tasks[this._index]));
    plotWindow?.webContents.send('plot:headers', this.currentTask.headers);
    return this._index;
  }

  public export() {
    return this._tasks;
  }
}

class Task {
  private _options: TaskOptions;
  private _buffer: Array<Array<number>>;
  private _plot_buffer: Array<Array<number>>;

  public get headers(): Array<string> {
    return this._options.headers;
  }

  constructor(options: TaskOptions) {
    this._buffer = Array<Array<number>>();
    this._plot_buffer = Array<Array<number>>();
    this._options = options;
  }

  public pushData(data: Array<number>) {
    this._buffer.push(data);
    if (this._plot_buffer.length > 200) {
      const average_log = new Array(this._plot_buffer[0].length).fill(0);

      this._plot_buffer.forEach((log) => {
        log.forEach((value, i) => {
          average_log[i] += value;
        });
      });

      for (let i = 0; i < average_log.length; ++i) average_log[i] = average_log[i] / this._plot_buffer.length;

      plotWindow?.webContents.send('plot:data', this._plot_buffer[0]);
      this._plot_buffer.splice(0);
    } else {
      this._plot_buffer.push(data);
    }
  }

  public export() {
    return this._buffer.slice();
  }
}

let log_manager: LogManager | null;

ipcMain.handle('log:start', (event, options: ExperimentOptions) => {
  log_manager = new LogManager(options);
});

ipcMain.handle('log:next', (event) => {
  log_manager?.next();
});

ipcMain.handle('log:stop', (event) => {
  log_manager?.export();
});

export { log_manager };
