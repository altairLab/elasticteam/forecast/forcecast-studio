import { app } from 'electron';
import * as fs from 'fs';
import * as path from 'path';

export const getPathFromString = (dirName: string) => {
  let toAppend = '';
  switch (dirName) {
    case 'task':
      toAppend = 'tasks';
      break;
    case 'log':
      toAppend = 'logs';
      break;
  }
  return path.join(app.getPath('userData'), toAppend);
};

export const listFiles = (dirPath: string | Buffer, extensions: Array<string>): Promise<string[]> => {
  return new Promise((resolve, reject) => {
    return fs.readdir(dirPath, { encoding: 'utf8', withFileTypes: true }, (err, files) => {
      if (err != null) reject(err);
      resolve(files.filter((file) => extensions.includes(path.extname(file.name))).map((file) => file.name));
    });
  });
};

export const readFile = (path: string) => {
  return new Promise((resolve, reject) => {
    return fs.readFile(path, (err, data) => {
      if (err != null) reject(err);
      resolve(data.toString('utf8'));
    });
  });
};

export const writeFile = (path: string, content: string) => {
  return new Promise((resolve, reject) => {
    return fs.writeFile(path, content, (err) => {
      if (err != null) reject(err);
      resolve(true);
    });
  });
};

export const deleteFile = (path: string) => {
  return new Promise((resolve, reject) => {
    return fs.unlink(path, (err) => {
      if (err != null) reject(err);
      resolve(true);
    });
  });
};

export const copyFile = (sourcePath: string, destinationPath: string) => {
  return new Promise((resolve, reject) => {
    return fs.copyFile(sourcePath, destinationPath, (err) => {
      if (err != null) reject(err);
      resolve(true);
    });
  });
};

export const renameFile = (sourcePath: string, destinationPath: string) => {
  return new Promise((resolve, reject) => {
    return fs.rename(sourcePath, destinationPath, (err) => {
      if (err != null) reject(err);
      resolve(true);
    });
  });
};

interface DirectoryMap {
  tasks: string;
  logs: string;
}
export class DirectoryManager {
  private _directory_map: DirectoryMap;

  constructor() {
    this._directory_map = {
      tasks: getPathFromString('task'),
      logs: getPathFromString('log'),
    };
  }

  public init() {
    Object.values(this._directory_map).forEach((path: string) => {
      if (!fs.existsSync(path)) {
        fs.mkdirSync(path, { recursive: true });
      }
    });
  }
}
