import { Identificators, PacketID } from './enums';
import { Transform } from 'stream';
import SerialPort from 'serialport';

interface SerialPacket {
  id: number;
  payload: Buffer;
  checksum: boolean;
}

const computeChecksum = (data: Buffer): number => {
  let sum = 0;
  for (const value of data) {
    sum += value;
  }
  return sum % 0xff;
};

const pack = (id: number, payload: Buffer) => {
  let output: Array<number> = [];
  output.push(Identificators.STX);
  output.push(id);
  output = output.concat(escape(payload));
  output.push(computeChecksum(payload));
  output.push(Identificators.ETX);
  return Buffer.from(output);
};

const unpack = (packet: Buffer): SerialPacket => {
  const packet_type = packet[1];
  const payloadArr = unescape(packet.slice(2, packet.length - 1));
  const checksum = payloadArr.pop();
  const payload = Buffer.from(payloadArr);
  return {
    id: packet_type,
    payload: payload,
    checksum: checksum === computeChecksum(payload),
  } as SerialPacket;
};

const escape = (data: Buffer) => {
  const output: Array<number> = [];
  let i = 0;
  while (i < data.length) {
    if ([Identificators.STX, Identificators.ETX, Identificators.ESC].includes(data[i])) {
      output.push(Identificators.ESC);
    }
    output.push(data[i]);
    i += 1;
  }
  return output;
};

const unescape = (data: Buffer) => {
  const output: Array<number> = [];
  let i = 0;
  while (i < data.length) {
    if (data[i] == Identificators.ESC && i + 1 < data.length) {
      output.push(data[i + 1]);
      i += 2;
    } else {
      output.push(data[i]);
      i += 1;
    }
  }
  return output;
};

const checkETX = (data: Buffer): boolean => {
  if (data.length < 2 || data[data.length - 1] != Identificators.ETX) return false;
  let escaped = false;
  for (let i = 2; data[data.length - i] == Identificators.ESC; ++i) {
    escaped = !escaped;
  }
  return !escaped;
};

class PacketParser extends Transform {
  // Parser which outputs packets coming from the input stream

  private _buffer: Buffer;

  constructor() {
    super();
    this._buffer = Buffer.alloc(0);
  }

  _transform(chunk, encoding, cb) {
    this._buffer = Buffer.concat([this._buffer, chunk]);

    let stx_index = this._buffer.indexOf(Identificators.STX);
    let etx_index = stx_index;

    while (stx_index >= 0 && etx_index < this._buffer.lastIndexOf(Identificators.ETX)) {
      etx_index = this._buffer.indexOf(Identificators.ETX, etx_index + 1);
      const possible_packet = this._buffer.slice(stx_index, etx_index + 1);

      if (checkETX(possible_packet)) {
        this.push(possible_packet);
        this._buffer = this._buffer.slice(etx_index + 1);
        etx_index = 0;
        stx_index = this._buffer.indexOf(Identificators.STX);
      }

    }
    //this._buffer = data;

    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    cb();
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  _flush(cb: Function) {
    this._buffer = Buffer.alloc(0);
    cb();
  }
}

class Queue {

  private _items: Array<Buffer>;

  constructor() {
    this._items = [];
  }

  enqueue(packet: Buffer) {
    this._items.push(packet);
  }

  dequeue() {
    this._items.shift();
  }

  peek() {
    return this._items[0];
  }

  clear() {
    this._items.splice(0);
  }

  size() {
    return this._items.length;
  }
}

class ForecastSerial {
  private _serial: SerialPort | null;
  private _parser: PacketParser | null;
  private devicePath: string | null;
  private queue: Queue;

  private queue_manager;

  private _waiting_receive: boolean;

  public onData: (packet: SerialPacket) => void;
  public onOpen: () => void;
  public onClose: () => void;

  constructor() {
    this._serial = null;
    this._parser = null;
    this.devicePath = null;
    this.queue = new Queue();
    this._waiting_receive = false;

    this.onOpen = () => {;};

    this.onClose = () => {;};

    this.onData = (packet: SerialPacket) => {;};
  }

  public get isOpen() {
    return this._serial?.isOpen ?? false;
  }

  open(path: string) {
    this.devicePath = path;
    this._serial = new SerialPort(path, { baudRate: 921600, autoOpen: true });
    this._parser = this._serial.pipe(new PacketParser());

    this._serial.on('open', () => {
      this._serial?.flush();
      console.log(`Serial open on ${this.devicePath ?? 'null'}`);
      this.onOpen();
    });

    this._serial.on('close', () => {
      console.log(`Serial close on ${this.devicePath ?? 'null'}`);
      this.onClose();
    });

    this._parser.on('data', (packet: Buffer) => {
      const unpacked = unpack(packet);
      if (!unpacked.checksum || unpacked.id == PacketID.NACK) {
        this._serial?.write(this.queue.peek());
      } else if (unpacked.id >= 0x60) {
        this.onData(unpacked);
      }
      else {
        this.queue.dequeue();
        this._waiting_receive = false;
        this.onData(unpacked);
      }
    });

    this.queue_manager = setInterval(() => {
      if (!this.isOpen) return;
      if (!this._waiting_receive && this.queue.size() > 0) {
        this._serial?.write(this.queue.peek());
        this._waiting_receive = true;
      }
    }, 10);
  }

  close() {
    clearInterval(this.queue_manager);
    this._serial?.close();
  }

  write(type: PacketID, payload: Buffer) {
    const packet: Buffer = pack(type, payload);
    this.queue.enqueue(packet);
    //return this._serial?.write(packet);
  }
}

const listDevices = () => {
  return SerialPort.list();
};

export { SerialPacket, ForecastSerial, listDevices };
