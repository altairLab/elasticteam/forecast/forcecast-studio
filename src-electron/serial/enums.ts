export const enum PacketID {
    ACK = 0x06,
    NACK = 0x0F,

    // Interrogations
    HW_INFO = 0x20,
    VER/*STAPPEN*/ = 0x21,
    CTRL_LIST = 0x22,
    REF_LIST = 0x23,
    CTRL_PARAMS = 0x24,
    REF_PARAMS = 0x25,
    CTRL_LOGS = 0x26,
    READY = 0x27,

    // Commands
    CTRL_SET = 0x40,
    HW_LOG_SET = 0x41,
    CTRL_LOG_SET = 0x42,
    REF_SET = 0x43,
    START_LOOP = 0x44,
    STANDBY = 0x45,

    // No response packets
    LOG = 0x60,
    ERROR = 0x61,
    END_EXP = 0x62
};

export enum Identificators {
    STX = 0x02,
    ETX = 0x03,
    ESC = 0x1b
};
