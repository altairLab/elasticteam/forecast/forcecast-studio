import { assert } from 'chai';
import { describe } from 'mocha';

import { RefinedTask } from '../../src/models/workflow';
import { TaskLoopMap } from '../../src/models/experiment';
import { refinedToLoopmap } from '../../src/components/conversions';

describe('RefinedTask -> TaskLoopMap', () => {
  it('Manual check', () => {
    const refined: RefinedTask = {
      testbed: {
        loggable: [],
        parameters: {},
      },
      controllers: {
        motor: {
          name: 'PositionPID',
          loggable: [],
          parameters: {
            KP: 0,
            KI: 0,
            KD: 0,
          },
        },
        environment: {
          name: 'Impedance Control',
          loggable: [],
          parameters: {
            J: 0,
          },
        },
      },
      references: {
        motor: {
          name: 'Step',
          parameters: {
            time: 0,
          },
        },
        environment: {
          name: 'Step',
          parameters: {
            time: 0,
          },
        },
      },
    };

    const loopMap: TaskLoopMap = {
      testbed: {
        testbed: {
          name: 'testbed',
          parameters: {},
        },
      },
      controllers: {
        motor: {
          name: 'PositionPID',
          parameters: {
            KP: {
              loop: false,
              value: '0',
            },
            KI: {
              loop: false,
              value: '0',
            },
            KD: {
              loop: false,
              value: '0',
            },
          },
        },
        environment: {
          name: 'Impedance Control',
          parameters: {
            J: {
              loop: false,
              value: '0',
            },
          },
        },
      },
      references: {
        motor: {
          name: 'Step',
          parameters: {
            time: {
              loop: false,
              value: '0',
            },
          },
        },
        environment: {
          name: 'Step',
          parameters: {
            time: {
              loop: false,
              value: '0',
            },
          },
        },
      },
    };

    assert.deepEqual(refinedToLoopmap(refined), loopMap);
  });
});
