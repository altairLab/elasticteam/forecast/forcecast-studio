import { cartesian } from '../../src/components/cartesian';
import { expect } from 'chai';
import { describe } from 'mocha';

describe('Cartesian product', () => {
  it('The Cartesian product of one array splits that array into arrays of one element', () => {
    const result = cartesian([[1, 2]]);
    const expected = [[1], [2]];
    expect(result).eql(expected);
  });

  it('Can create a Cartesian product of two arrays', () => {
    const result = cartesian([
      [1, 2],
      [3, 4],
    ]);
    const expected = [
      [1, 3],
      [1, 4],
      [2, 3],
      [2, 4],
    ];
    expect(result).eql(expected);
  });

  it('Can create a Cartesian product of more than two arrays', () => {
    const result = cartesian([
      [1, 2],
      [10, 20],
      [100, 200, 300],
    ]);
    const expected = [
      [1, 10, 100],
      [1, 10, 200],
      [1, 10, 300],
      [1, 20, 100],
      [1, 20, 200],
      [1, 20, 300],
      [2, 10, 100],
      [2, 10, 200],
      [2, 10, 300],
      [2, 20, 100],
      [2, 20, 200],
      [2, 20, 300],
    ];
    expect(result).eql(expected);
  });
});
