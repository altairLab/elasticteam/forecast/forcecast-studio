# ForceCAST Studio

### Build from Source

Clone this repository and install all dependencies.
```bash
git clone https://gitlab.com/altairLab/elasticteam/forecast/forcecast-studio.git
cd forcecast-studio
npm install
```

Actually build the app, the executables are found in the newly-created 'dist' directory.
```bash
quasar build -m electron
```
