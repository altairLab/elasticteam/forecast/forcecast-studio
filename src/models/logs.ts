export interface ExperimentOptions {
  experimentName: string;
  length: number;
  tasks: Array<TaskOptions>
}

export interface TaskOptions {
  headers: Array<string>;
}

export interface ExperimentDir {
  name: string;
  path: string;
  logs: Array<string>;
}

export interface CSVOptions {
  headers: Array<string>;
  data: Array<Array<number>>;
}