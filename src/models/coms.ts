export interface SerialPacket {
    id: number,
    payload: Buffer
};
