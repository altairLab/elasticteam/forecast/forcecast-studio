import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { ExperimentStateInterface } from './state';

const actions: ActionTree<ExperimentStateInterface, StateInterface> = {
  someAction (/* context */) {
    // your code
  }
};

export default actions;
