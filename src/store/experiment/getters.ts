import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { ExperimentStateInterface } from './state';

const getters: GetterTree<ExperimentStateInterface, StateInterface> = {
  getBlockById: (state) => (id) => state.currentExperiment.blocks.find(block => block.id === id)
};

export default getters;
