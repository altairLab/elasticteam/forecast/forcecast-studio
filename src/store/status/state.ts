export interface StatusStateInterface {
  connected: boolean
}

function state(): StatusStateInterface {
  return {
    connected: false
  };
}

export default state;
