import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { StatusStateInterface } from './state';

const getters: GetterTree<StatusStateInterface, StateInterface> = {
  getConnection: (state) => state.connected,
};

export default getters;
