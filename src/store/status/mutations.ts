import { MutationTree } from 'vuex';
import { StatusStateInterface } from './state';

const mutation: MutationTree<StatusStateInterface> = {
  setConnection(state: StatusStateInterface, payload: boolean) {
    state.connected = payload;
  },
};

export default mutation;
