export enum EnumStatus {
  MISSING = 0,
  ERROR = 1,
  READY = 2,
}
