### Node

A Node object is a blueprint necessary for the creation of a workspace block.

```typescript
{
  name: name,
  family: string // node group, e.g.: controller, reference, testbed
  inputs: Array<string>, // list of input slots
  outputs: Array<string>, // list of output slots
  loggable: Array<string>, // loggables parameters
  data: Array<string>, // parameters
}
```
