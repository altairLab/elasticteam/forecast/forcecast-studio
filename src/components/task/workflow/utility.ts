import { Point2D, RefinedBlock } from 'src/models/workflow';

export function getOffsetRect(element: HTMLElement) {
  const box = element.getBoundingClientRect();

  const scrollTop = window.pageYOffset;
  const scrollLeft = window.pageXOffset;

  const top = box.top + scrollTop;
  const left = box.left + scrollLeft;

  return {
    top: Math.round(top),
    left: Math.round(left),
  };
}

export function getMousePosition(element: HTMLElement, event: MouseEvent): Point2D {
  const mouseX = event.pageX || event.clientX + document.documentElement.scrollLeft;
  const mouseY = event.pageY || event.clientY + document.documentElement.scrollTop;

  const offset = getOffsetRect(element);
  const x = mouseX - offset.left;
  const y = mouseY - offset.top;

  return {
    x: x,
    y: y,
  };
}

export function getSlotPosition(block: RefinedBlock, slotIndex: number, type: string): Point2D {

  if (!block || slotIndex === -1) {
    return {x: 0, y: 0}
  }

  const WIDTH = 150;
  const HEIGHT = 140;
  let x = block.position.x;
  let y = block.position.y;
  let step = 0;

  switch (type) {
    case 'output':
      step = HEIGHT / (block.outputs.length + 1);
      break;
    case 'input':
      step = HEIGHT / (block.inputs.length + 1);
  }

  switch (type) {
    case 'output':
      x += WIDTH;
    case 'input':
      y += (slotIndex + 1) * step;
  }
  return { x: x, y: y };
}
