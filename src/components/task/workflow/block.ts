import { Node, RawBlock, RefinedBlock, Slot, Data, Loggable } from 'src/models/workflow';
import { get_node, get_node_list, add_controller, add_reference } from './nodes';
import { v4 as uuidv4 } from 'uuid';

export const create_block = (node: Node, options?: RawBlock): RefinedBlock => {
  const inputs: Array<Slot> = node.inputs.map((input_name: string): Slot => {
    return {
      name: input_name,
      active: '',
    };
  });

  const outputs: Array<Slot> = node.outputs.map((output_name: string): Slot => {
    return {
      name: output_name,
      active: '',
    };
  });

  const loggable = <Loggable>{};
  node.loggable.forEach((loggable_parameter: string) => {
    loggable[loggable_parameter] = false;
  });

  const data = <Data>{};
  node.data.forEach((value_parameter: string) => {
    data[value_parameter] = 0;
  });

  return {
    id: options?.id || uuidv4(),
    name: node.name,
    position: options?.position || { x: 0, y: 0 },
    family: node.family,
    inputs: inputs,
    outputs: outputs,
    loggable: loggable,
    data: data,
  };
};
