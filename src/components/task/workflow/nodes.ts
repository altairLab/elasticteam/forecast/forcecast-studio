import { Node, NodeUpdate } from 'src/models/workflow';

const node_list: Array<Node> = [/*
  {
    name: 'PositionPID',
    family: 'controller',
    inputs: [''],
    outputs: [''],
    loggable: ['output'],
    data: ['KP', 'KI', 'KD'],
  },
  {
    name: 'VelocityPID',
    family: 'controller',
    inputs: ['ref'],
    outputs: ['output'],
    loggable: ['output'],
    data: ['KP', 'KI', 'KD'],
  },
  {
    name: 'Impedance Control',
    family: 'controller',
    inputs: ['ref'],
    outputs: ['output'],
    loggable: ['output'],
    data: ['J'],
  },
  {
    name: 'Step',
    family: 'reference',
    inputs: [],
    outputs: [''],
    loggable: [],
    data: [],
  },
  {
    name: 'Sinusoid',
    family: 'reference',
    inputs: [],
    outputs: ['value'],
    loggable: [],
    data: [],
  },
  {
    name: 'Sweep',
    family: 'reference',
    inputs: [],
    outputs: ['value'],
    loggable: [],
    data: [],
  },
  {
    name: 'Constant',
    family: 'reference',
    inputs: [],
    outputs: ['value'],
    loggable: [],
    data: [],
  },
  {
    name: 'Ramp',
    family: 'reference',
    inputs: [],
    outputs: ['value'],
    loggable: [],
    data: [],
  },
  {
    name: 'Testbed 1',
    family: 'testbed',
    inputs: ['motor'],
    outputs: [],
    loggable: [],
    data: ['Frequency'],
  },
  {
    name: 'Testbed 2',
    family: 'testbed',
    inputs: ['motor', 'environment'],
    outputs: [],
    loggable: [],
    data: [],
  },*/
];

const get_node_list = (family?: string) => {
  if (!family) return node_list;
  return node_list.filter((node: Node) => node.family === family);
};

const get_node = (node_name: string, node_family: string) => {
  const result = get_node_list(node_family).find((node: Node) => node.name === node_name);
  return result;
};

const add_node_to_list = (node_to_add: Node) => {
  const temp_node = get_node(node_to_add.name, node_to_add.family);
  if (!temp_node) {
    node_list.push(node_to_add);
    return node_to_add;
  } else {
    return null;
  }
};

const update_node = (update: NodeUpdate) => {
  const nodeToUpdate = get_node(update.name, update.family);
  if (!nodeToUpdate) return;
  nodeToUpdate.inputs = (update.inputs)? update.inputs : nodeToUpdate.inputs;
  nodeToUpdate.outputs = (update.outputs)? update.outputs : nodeToUpdate.outputs;
  nodeToUpdate.loggable = (update.loggable)? update.loggable : nodeToUpdate.loggable;
  nodeToUpdate.data = (update.data)? update.data : nodeToUpdate.data;
}

const add_controller = (name: string, parameters: Array<string>, loggable: Array<string>) => {
  return add_node_to_list({
    name: name,
    family: 'controller',
    inputs: [''],
    outputs: [''],
    loggable: loggable,
    data: parameters,
  });
};

const add_reference = (name: string, parameters: Array<string>) => {
  return add_node_to_list({
    name: name,
    family: 'reference',
    inputs: [],
    outputs: [''],
    loggable: [],
    data: parameters,
  });
};

const add_hardware = (name: string, motorsNames: Array<string>, loggable: Array<string>) => {

  return add_node_to_list({
    name: name,
    family: 'testbed',
    inputs: motorsNames,
    outputs: [],
    loggable: loggable,
    data: ['Duration', 'Frequency'],
  });
};

const clear = () => {
  node_list.splice(0)
}

/* const controller = add_controller('PID', ['KP', 'KI', 'KD'], []);
update_node({
  name: 'PID',
  family: 'controller',
  loggable: ['out', 'dout', 'ddout']
}); */

//add_hardware('Hardware', ['motor', 'environment'], []);

export { get_node, get_node_list, add_controller, add_reference, add_hardware, update_node, clear };
