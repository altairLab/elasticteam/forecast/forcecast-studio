import { Notify } from 'quasar';

export const errorNotification = (message: string) => {
  Notify.create({
    type: 'negative',
    position: 'top',
    classes: 'notification',
    message: message,
  });
};

export const successNotification = (message: string) => {
  Notify.create({
    type: 'positive',
    position: 'top',
    classes: 'notification',
    message: message,
  });
};
