/* eslint-disable @typescript-eslint/no-namespace */
import { ExperimentOptions } from 'src/models/logs';
import { RefinedTask } from 'src/models/workflow';
import { extractLoggables } from './scripts';


export class Logger {

  // TODO: pass only the experiment name
  public static async start (queue: Array<RefinedTask>) {
    const options: ExperimentOptions = {
      experimentName: '',
      length: queue.length,
      tasks: queue.map(task => {
        return {
          headers: extractLoggables(task)
        }
      })
    }
    await window.log.init(options)
  }

  // TODO: pass the next task
  public static async next (task: RefinedTask) {
    await window.log.next();
  }

  public static async stop () {
    await window.log.stop();
  }

}
