import { Serial } from './serial';
import { SessionStorage } from 'quasar'
import { add_controller, add_reference, add_hardware, update_node, clear } from 'components/task/workflow/nodes'
import { RefinedTask } from 'src/models/workflow';


export interface MotorMap {
  [motorNumber: number]: string
}

export const number2motorName: MotorMap = {
  0: 'motor',
  1: 'environment',
  2: 'other'
}

export const motorName2number = {
  motor: 0,
  environment: 1,
  other: 2
}


export const firstTimeSetup = () => {
  Serial.getVERsion((version: string) => {
    SessionStorage.set('FW', version);
  });

  clear();

  // Created HW node
  Serial.getHWInfo((motorsNumber: number, HWLoggables: Array<string>) => {
    const motorsNames = Array<string>();
    for (let i = 0; i < motorsNumber; ++i) {
      motorsNames.push(number2motorName[i]);
    }
    add_hardware('Testbed', motorsNames, HWLoggables);
  });

  // CTRLs nodes
  Serial.getCTRLList((controllersList: Array<string>) => {
    controllersList.forEach(controllerName => {
      add_controller(controllerName, [], []);
       Serial.getCTRLParameters(controllerName, (ctrlName: string, parameters: Array<string>) => {
        update_node({
          name: controllerName,
          family: 'controller',
          data: parameters
        });
      });

    Serial.getCTRLLoggables(controllerName, (ctrlName: string, loggables: Array<string>) => {
        update_node({
          name: ctrlName,
          family: 'controller',
          loggable: loggables
        });
      });

    });
  });

  // Create REFs nodes
  Serial.getREFList((referencesList: Array<string>) => {
    referencesList.forEach(referenceName => {
      if (referenceName.length === 0) return;
      Serial.getREFParameters(referenceName, (refName: string, parameters: Array<string>) => {
        add_reference(refName, parameters);
      });
    });
  });
};

const sleep = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Send all required informations to the board to setup a task and start it
export const setTask = (task: RefinedTask) => {

  Serial.setHWLoggables(task.testbed.loggable);

  Object.entries(task.controllers).forEach((entry) => {
    const [motorName, controller] = entry;
    Serial.setCTRL(controller.name, motorName2number[motorName], Object.values(controller.parameters));
    Serial.setCTRLLoggables(motorName2number[motorName], controller.loggable);
  });

  Object.entries(task.references).forEach(entry => {
    const [motorName, reference] = entry;
    Serial.setREF(reference.name, motorName2number[motorName], Object.values(reference.parameters))
  });
}

export const startTask = (task:RefinedTask, onTaskEnd?: () => void) => {
  setTask(task);
  const taskDuration = task.testbed.parameters['Duration'];
  const taskFrequency = task.testbed.parameters['Frequency'];
  Serial.getReady((ready: boolean) => {
    console.log('Testbed ready: ', ready);
    if (ready) {
      Serial.startLoop(taskFrequency, taskDuration, onTaskEnd);
    }
  });
}

export const extractLoggables = (task: RefinedTask) => {
  let result = Array<string>();
  result = result.concat(task.testbed.loggable);
  Object.values(task.controllers).forEach(controller => {
    result = result.concat(controller.loggable);
  });
  return result;
}